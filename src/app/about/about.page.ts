import { Component, OnInit } from '@angular/core';
import { TranslateConfigService } from '../translate-config.service';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  selectedLanguage: string;
  constructor(private translateConfigService: TranslateConfigService, public helper: HelperService) { 
    this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
  }

  ionViewDidEnter(){
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit() {
  }

}
