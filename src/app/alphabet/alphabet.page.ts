import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';
import { TranslateConfigService } from '../translate-config.service';
import { Storage } from '@ionic/storage';
import { HelperService } from '../services/helper.service';


@Component({
  selector: 'app-alphabet',
  templateUrl: './alphabet.page.html',
  styleUrls: ['./alphabet.page.scss'],
})
export class AlphabetPage implements OnInit {

  selectedLanguage: string;
  items: any;
  cur = 1;
  @ViewChild(IonSlides, { static: true }) slides: IonSlides;
  constructor(private storage:Storage, private nav: NavController, private translateConfigService: TranslateConfigService, public helper: HelperService) { 
    this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
  }

  ionViewDidEnter(){
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit() {
    this.slides.lockSwipes(true);
  	this.items = [
  		{
  			letter: "a",
  			fpro: "aa",
  			epro: "aah"
  		},
  		{
  			letter: "b",
  			fpro: "bè",
  			epro: "beh"
  		},
  		{
  			letter: "d",
  			fpro: "dè",
  			epro: "deh"
  		},
  		{
  			letter: "e",
  			fpro: "é",
  			epro: "ay"
  		},
  		{
  			letter: "ɛ",
  			fpro: "è",
  			epro: "err"
  		},
  		{
  			letter: "f",
  			fpro: "fè",
  			epro: "feh"
  		},
  		{
  			letter: "g",
  			fpro: "gè",
  			epro: "geh"
  		},
  		{
  			letter: "h",
  			fpro: "hè",
  			epro: "heh"
  		},
  		{
  			letter: "i",
  			fpro: "ee",
  			epro: "ee"
  		},
  		{
  			letter: "k",
  			fpro: "kè",
  			epro: "keh"
  		},
  		{
  			letter: "l",
  			fpro: "il",
  			epro: "ll"
  		},
  		{
  			letter: "m",
  			fpro: "mm",
  			epro: "mm"
  		},
  		{
  			letter: "n",
  			fpro: "nn",
  			epro: "nn"
  		},
  		{
  			letter: "o",
  			fpro: "eau",
  			epro: "oo"
  		},
  		{
  			letter: "ɔ",
  			fpro: "or",
  			epro: "ore"
  		},
  		{
  			letter: "p",
  			fpro: "pè",
  			epro: "peh"
  		},
  		{
  			letter: "r",
  			fpro: "hrr",
  			epro: "hrr"
  		},
  		{
  			letter: "s",
  			fpro: "sè",
  			epro: "seh"
  		},
  		{
  			letter: "t",
  			fpro: "tè",
  			epro: "teh"
  		},
  		{
  			letter: "u",
  			fpro: "uu",
  			epro: "uu"
  		},
  		{
  			letter: "w",
  			fpro: "wè",
  			epro: "weh"
  		},
  		{
  			letter: "y",
  			fpro: "yè",
  			epro: "yea"
  		},
  	];
  }

  play(url){
  	var audio = new Audio('assets/sounds/alphabet/' + url + '.mp3');
    audio.play();
  }

  next(){
    this.slides.lockSwipes(false);
    this.slides.slideNext();
	  this.cur+=1;
    this.slides.lockSwipes(true);
  }

  previous(){
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
	  if(this.cur>1){
		  this.cur-=1;
	  }
    this.slides.lockSwipes(true);
  }

  tostart(){
    this.slides.lockSwipes(false);
    this.slides.slideTo(0);
    this.cur = 1;
    this.slides.lockSwipes(true);
  }

  stop(){
    this.nav.pop();
  }

  toend(){
    this.slides.lockSwipes(false);
    this.cur = 24;
    this.slides.slideTo(26);
    this.slides.lockSwipes(true);
  }

  quiz(type){ 
    this.helper.navQuiz(type);
  }

}
