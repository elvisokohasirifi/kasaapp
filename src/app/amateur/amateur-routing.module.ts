import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AmateurPage } from './amateur.page';

const routes: Routes = [
  {
    path: '',
    component: AmateurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AmateurPageRoutingModule {}
