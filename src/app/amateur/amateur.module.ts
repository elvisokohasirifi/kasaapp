import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { AmateurPageRoutingModule } from './amateur-routing.module';

import { AmateurPage } from './amateur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AmateurPageRoutingModule,
    TranslateModule.forChild() 
  ],
  declarations: [AmateurPage]
})
export class AmateurPageModule {}
