import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AmateurPage } from './amateur.page';

describe('AmateurPage', () => {
  let component: AmateurPage;
  let fixture: ComponentFixture<AmateurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmateurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AmateurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
