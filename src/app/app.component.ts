import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, IonRouterOutlet, NavController, AlertController, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { TranslateConfigService } from './translate-config.service';
import { TranslateService } from '@ngx-translate/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { HelperService } from './services/helper.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  selectedLanguage: string;
  @ViewChild(IonRouterOutlet, {static: false}) routerOutlet: IonRouterOutlet;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private nav: NavController,
    private router: Router,
    private storage: Storage,  
    private translateConfigService: TranslateConfigService, 
    private alertController: AlertController,
    private translate: TranslateService,
    public ngFireAuth: AngularFireAuth,
    public loading: LoadingController,
    public helper: HelperService,
  ) {
    this.initializeApp();
    this.selectedLanguage = this.translateConfigService.currentLang();
    this.helper.loadall();
    this.translate.setDefaultLang(this.helper.selectedLanguage);
    this.platform.backButton.subscribe(() => {
      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      } else {
        this.confirmClose();
      }
    });
  }

  async confirmClose(){
    let msg = '';
    if(this.helper.selectedLanguage && this.helper.selectedLanguage==='fr'){
      msg = 'Voulez-vous vraiment fermer cette application?';
    }else{
      msg = 'Are you sure you want to close this app?';
    }
    const alert = await this.alertController.create({
        message: msg,
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            cssClass: 'dark',
            handler: (blah) => {
              //console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Yes',
            handler: () => {
              navigator['app'].exitApp();
            }
          }
        ]
      });

      await alert.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      let a = this.nav;
      this.ngFireAuth.onAuthStateChanged(function(user) {
        //alert(JSON.stringify(user));
        if (user) {
          a.navigateRoot('tabs/tab1');
        } else {
          a.navigateRoot('welcome');
        }
      });
      var notificationOpenedCallback = function(jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        //this.nav.navigateForward('/notifications');
      };

      // Set your iOS Settings
      var iosSettings = {};
      iosSettings["kOSSettingsKeyAutoPrompt"] = false;
      iosSettings["kOSSettingsKeyInAppLaunchURL"] = false;
  
      window["plugins"].OneSignal
        .startInit("5fdd3516-66c5-4ab4-96a0-2169b2d5db07")
        .handleNotificationOpened(notificationOpenedCallback)
        .iOSSettings(iosSettings)
        .inFocusDisplaying(window["plugins"].OneSignal.OSInFocusDisplayOption.Notification)
        .endInit();

      window["plugins"].OneSignal.promptForPushNotificationsWithUserResponse(function(accepted) {
        console.log("User accepted notifications: " + accepted);
      });
    });
  }
}
