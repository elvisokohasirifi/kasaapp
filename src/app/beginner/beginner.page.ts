import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { TranslateConfigService } from '../translate-config.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-beginner',
  templateUrl: './beginner.page.html',
  styleUrls: ['./beginner.page.scss'],
})
export class BeginnerPage implements OnInit {
	selectedLanguage:string;
  constructor(private storage:Storage, private nav: NavController, 
    private translateConfigService: TranslateConfigService) { 
    this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
  }

  ngOnInit() {
  }

  navigate(url){
    this.nav.navigateForward(url);
  }
}
