import { Component, OnInit,ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HelperService } from '../services/helper.service';
import { TranslateConfigService } from '../translate-config.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {
  @ViewChild('email', { static: true }) email;
  error;
  selectedLanguage:string;

  constructor(private translateConfigService: TranslateConfigService, public helper: HelperService, private storage: Storage) { 
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ionViewDidEnter(){
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit() {
    this.email.value = this.helper.email;
  }

  sendlink(){
    this.helper.forgotpassword(this.email.value);
  }

}
