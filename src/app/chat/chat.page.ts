import { Component, OnInit, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, IonContent } from '@ionic/angular';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from '@angular/fire/firestore';
import { TranslateConfigService } from '../translate-config.service';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  messages: any;
  room: any;
  name: string;
  roomname: string;
  id: string;
  selectedLanguage;

  @ViewChild('message', {static: true}) message: any;
  @ViewChild(IonContent, {read: IonContent, static: false}) myContent: IonContent;

  constructor(
    private storage: Storage, 
    public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth, 
    private nav: NavController,
    private translateConfigService: TranslateConfigService, 
    public helper: HelperService) {
      this.selectedLanguage = this.translateConfigService.currentLang();
    }
    
    ionViewDidEnter(){
      this.selectedLanguage = this.translateConfigService.currentLang();
    }

  async ngOnInit() {
	  await Promise.all([this.storage.get("name"), this.storage.get("room"), this.storage.get("userid")
      ]).then(values => {
        this.name = values[0];
        if (!values[1]) {
          this.nav.pop();
        }
        else{
          this.room = JSON.parse(values[1]);
          this.roomname = this.room.title;
        }
        if (values[2]) {
          this.id = values[2];
        }
        else{
          this.nav.navigateRoot('welcome');
        }
    }); 
	 
      var mess = this.afStore.collection('/forums/' + this.room.id + '/messages', ref => ref.orderBy('date', 'asc'));
  	  mess.snapshotChanges().subscribe(res=>{
      if(res){
        this.messages = res.map(e=>{
          return{
            id: e.payload.doc.id,
            sender: e.payload.doc.data()['sender'],
            senderid: e.payload.doc.data()['senderid'],
            message: e.payload.doc.data()['message'],
            date: e.payload.doc.data()['date'],
          }
        }) 
        this.ScrollToBottom();  
      }  
      else{
      	this.messages = new Array();
      }
    }), error=>{
      if(this.selectedLanguage === "en"){
        alert('Something went wrong with adding user to firestore: ' + JSON.stringify(error));
      }else{
        alert("Une erreur s'est produite lors de l'ajout de l'utilisateur à firestore: " + JSON.stringify(error));
      }
      return;
    }
  }

  sendMessage(){
  	if (this.message.value.trim() == '') {
  		return;
  	}
  	let n = new Date();
  	let k = Date.UTC(n.getFullYear(), n.getMonth(), n.getDate(), n.getHours(), n.getMinutes(), n.getMilliseconds());
    this.afStore.collection('/forums/' + this.room.id + '/messages').add({
        sender: this.name,
        senderid: this.id,
        message: this.message.value,
        date: k
    });
    this.message.value = '';
    this.ScrollToBottom();
  }

  ScrollToBottom(){
    setTimeout(() => {
      this.myContent.scrollToBottom(300);
   }, 500);

  }

  calc(name){
    let split = name.toUpperCase().split(' ');
    if (split.length == 1) {
      return name[0];
    }
    else if (split.length > 1) {
      return split[0][0] + '' + split[1][0];
    }
    else{
      return '?';
    }
  }

  convertdate(timestamp){
    return new Date(timestamp).toUTCString();
  }

}
