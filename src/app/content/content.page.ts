import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';
import { TranslateConfigService } from '../translate-config.service';
import { HelperService } from '../services/helper.service';


@Component({
  selector: 'app-content',
  templateUrl: './content.page.html',
  styleUrls: ['./content.page.scss'],
})
export class ContentPage implements OnInit {
  item;
  index;
  items: any;
  sounds;
  subidx;
  cur = 1;
  lessons = this.helper.lessons.content;
  selectedLanguage: string;
  content;
  currentIndex;
  @ViewChild(IonSlides, { static: true }) slides: IonSlides;
  constructor(private nav: NavController, 
    private translateConfigService: TranslateConfigService, 
    public helper: HelperService) { 
      if(this.helper.subidx!=null){
        this.item = this.lessons[this.helper.index].children[this.helper.subidx];
      }else{
        this.item = this.lessons[this.helper.index];
      }
      this.sounds = this.lessons[this.helper.index].sounds;
  }

    getSlideIndex(){
      this.slides.getActiveIndex().then(
        (index)=>{
          this.currentIndex = index;
      });
    }

  ionViewDidEnter(){
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit() {
    this.slides.lockSwipes(true);
    this.items = this.item.content;
    this.content = this.item;
  }

  play(url){
    var audio = new Audio(this.sounds + url);
    audio.play();
  }

  next(){
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.cur+=1;
    this.slides.lockSwipes(true);
  }

  previous(){
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    if(this.cur>1){
      this.cur-=1;
    }
    this.slides.lockSwipes(true);
  }

  tostart(){
    this.slides.slideTo(0);
  }

  stop(){
    this.nav.pop();
  }

  quiz(type){ 
    this.helper.navQuiz(type);
  }

  toend(){
    this.slides.length().then(length => {
      this.slides.slideTo(length-1);
    });
  }


}
