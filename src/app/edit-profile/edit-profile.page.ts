import { Component, OnInit, ViewChild } from '@angular/core';
import { HelperService } from '../services/helper.service';
import { Storage } from '@ionic/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, NavController } from '@ionic/angular';
import { TranslateConfigService } from '../translate-config.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  @ViewChild('name', { static: true }) name;
  @ViewChild('email', { static: true }) email;
  @ViewChild('phone', { static: true }) phone;
  error;
  ishidden: boolean;
  selectedLanguage:string;
  uid;

  constructor(
    private translateConfigService: TranslateConfigService, 
    public helper: HelperService, 
    private storage: Storage,
    private nav: NavController,
    public afStore: AngularFirestore, 
    private loading: LoadingController) { 
    this.selectedLanguage = this.translateConfigService.currentLang();
  }
  
  ionViewDidEnter(){
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit() {
    this.ishidden = true;
  	this.name.value = this.helper.name;
  	this.email.value = this.helper.email;
  	this.phone.value = this.helper.phone;
    this.uid = this.helper.userid;
  }


  async save(){

    this.error = '';
  	if (this.name.value == '' || this.name.value.length < 3 ) {
      if(this.selectedLanguage==='en'){
        this.error = 'Please enter a valid name';
      }else{
        this.error = "Merci d'entrer un nom valide";
      }
      return;
    }  
    else if (this.phone.value == null) {
      if(this.selectedLanguage==='en'){
        this.error = 'Please enter a valid phone number';
      }else{
        this.error = "S'il vous plaît entrer un numéro de téléphone valide";
      }
      return;
    }
    
    let msg = '';
    if(this.selectedLanguage==='en'){
      msg = 'Saving. Please wait…😊';
    }else{
      msg = "enregistrement. S'il vous plaît, attendez…😊";
    }
    let loader = await this.loading.create({
      message: msg,
    });
    loader.present().then(() => {
      var docRef = this.afStore.collection('users').doc(this.uid);
        docRef.get().toPromise().then(doc => {
          if (doc.exists) {
            loader.dismiss();
            docRef.update({
                name: this.name.value,
                phone: this.phone.value
            });
            this.storage.set('name', this.name.value);
            this.storage.set('phone', this.phone.value);
            this.nav.pop();
            if(this.selectedLanguage==='en'){
              alert('Saved');
            }else{
              alert('Enregistré');
            }

          } 
          else{
            loader.dismiss();
            if(this.selectedLanguage==='en'){
              alert('You cannot save your data. You are signed in anonymously');
            }else{
              alert('Vous ne pouvez pas enregistrer vos données. Vous êtes connecté anonymement');
            }
          }
        })
       .catch(error => {
           loader.dismiss();
            if(this.selectedLanguage==='en'){
              alert('An error occurred, please try again later ' + JSON.stringify(error));
            }else{
              alert('Un erreur est survenue, veuillez réessayer plus tard ' + JSON.stringify(error))
            }
            return;
        });
     });
  }

}
