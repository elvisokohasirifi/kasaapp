import { Component, OnInit, ViewChild } from '@angular/core';
import { HelperService } from '../services/helper.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController } from '@ionic/angular';
import { TranslateConfigService } from '../translate-config.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {
  @ViewChild('message', { static: true }) message;
  selectedLanguage: string;
  error;
  uid;
  

  constructor(
    private translateConfigService: TranslateConfigService, 
    public helper: HelperService, 
    public afStore: AngularFirestore, 
    private loading: LoadingController) { 
    this.selectedLanguage = this.translateConfigService.currentLang();
  }
  
  ionViewDidEnter(){
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit() {
    this.message.value = '';
    this.uid = this.helper.userid;
  }

  async savefeedback(){
    this.error = '';
  	if (this.message.value == '') {
      if(this.selectedLanguage==='en'){
        this.error = 'Message is empty';
      }else{
        this.error = 'Le message est vide';
      }
      return;
    }

    let msg = '';
    if(this.selectedLanguage==='en'){
      msg = 'Sending feedback...';
    }else{
      msg = 'Envoi de commentaires...';
    }
    let loader = await this.loading.create({
      message: msg, 
    });

    this.afStore.collection('feedback').add({
          title: 'User Feedback',
          comment: this.message.value,
          timestamp: Date()
      })
      .catch(error => {
        if(this.selectedLanguage==='en'){
          alert('Something went wrong with saving feedback: ' + JSON.stringify(error));
        }else{
          alert("Une erreur s'est produite lors de l'enregistrement des commentaires: " + JSON.stringify(error))
        }
        return;
    });

    loader.present().then(() => {
      loader.dismiss();
      if(this.selectedLanguage==='en'){
        alert('Thank you for the feedback!');
      }else{
        alert('Merci pour votre retour!');
      }
      this.helper.back();
    });
  }
}
