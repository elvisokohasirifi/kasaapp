import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { ForumsPageRoutingModule } from './forums-routing.module';

import { ForumsPage } from './forums.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ForumsPageRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [ForumsPage]
})
export class ForumsPageModule {}
