import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from '@angular/fire/firestore';
import { TranslateConfigService } from '../translate-config.service';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-forums',
  templateUrl: './forums.page.html',
  styleUrls: ['./forums.page.scss'],
})
export class ForumsPage implements OnInit {

  rooms: any;
	name;
  selectedLanguage;

  constructor(
    private storage: Storage, 
    public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth, 
    private nav: NavController, 
    private translateConfigService: TranslateConfigService, 
    public helper: HelperService) {
      this.selectedLanguage = this.translateConfigService.currentLang();
      Promise.all([this.storage.get("name")]).then(values => {
      this.name = values[0];
	});
}

ionViewDidEnter(){
  this.selectedLanguage = this.translateConfigService.currentLang();
}

  async ngOnInit() {
  	this.afStore.collection('/forums/').snapshotChanges().subscribe(res=>{
      if(res){
        this.rooms = res.map(e=>{
          return{
            id: e.payload.doc.id,
            title: e.payload.doc.data()['title'],
            creator: e.payload.doc.data()['creator']
          }
        })   
      }  
      else{
      	alert(JSON.stringify(res));
      }
    })
    , error => {
      if(this.selectedLanguage === "en"){
        alert('Something went wrong with adding user to firestore: ' + JSON.stringify(error));
      }else{
        alert("Une erreur s'est produite lors de l'ajout de l'utilisateur à firestore: " + JSON.stringify(error));
      }
      return;
  	}
  }

  navigate(room){
  	this.storage.set('room', JSON.stringify(room));
  	this.nav.navigateForward('chat');
  }

  add(){
  	var r = '';
    if(this.selectedLanguage==='fr'){
      r = prompt('Veuillez saisir un titre pour ce forum de discussion');
    }else{
      r = prompt('Please enter a title for this discussion forum');
    }
  	if (r) {
  		let n = new Date();
  		let k = Date.UTC(n.getFullYear(), n.getMonth(), n.getDate(), n.getHours(), n.getMinutes(), n.getMilliseconds());
  		this.afStore.collection('/forums/').add({
	        title: r,
	        creator: this.name,
	        date: k
	    });
  	}
  }

}
