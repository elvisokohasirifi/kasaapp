import { Component, OnInit, ViewChild } from '@angular/core';
import { HelperService } from '../services/helper.service';
import { TranslateConfigService } from '../translate-config.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild('email', { static: true }) email;
  @ViewChild('password', { static: true }) password;
  error;
  ishidden: boolean;
  selectedLanguage:string;

  constructor(private translateConfigService: TranslateConfigService, public helper: HelperService) { 
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit() {
  	this.ishidden = true;
  }

  login(){
  	this.error = '';
  	if (this.email.value == '') {
      this.error = this.selectedLanguage === 'en' ? 'Please enter a valid email' : 'Entrez un e-mail valide';
      return;
    }
    else if (this.password.value.length < 6) {
      this.error = this.selectedLanguage === 'en' ? 'Password must have at least 6 characters' : 'Le mot de passe doit avoir au moins 6 caractères';
      return;
    }
    else if (this.password.value == '') {
      this.error = this.selectedLanguage === 'en' ? 'Please enter a valid password': 'Entrez un mot de passe valide';
      return;
    }
    else {
    	this.ishidden = false;
    	this.helper.login(this.email.value, this.password.value);
    	this.ishidden = true;
    }
  }

}
