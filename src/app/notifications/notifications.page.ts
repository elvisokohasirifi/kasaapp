import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { TranslateConfigService } from '../translate-config.service';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  notifs: any;
  access_token: string;
  selectedLanguage: string;
  @ViewChild('search', { static: true }) search;
    public ishidden = true;
  public mainslides: any;

  constructor(private nav: NavController, private alertController: AlertController,
    private translateConfigService: TranslateConfigService, 
    private storage: Storage, private loading: LoadingController, private HTTP: HTTP) {
      this.selectedLanguage = this.translateConfigService.currentLang();
      Promise.all([this.storage.get("access_token")]).then(values => {
        this.access_token = values[0];
      });
    }

    ionViewDidEnter(){
      this.selectedLanguage = this.translateConfigService.currentLang();
    }

  ngOnInit() {
    this.storage.get('notifications').then((val) => {
      if(val){
        this.notifs = JSON.parse(val);
        this.mainslides = this.notifs;
      }
      else{
        this.refresh();
      }
    });
  }

  filterItems(){
    var searchTerm = this.search.value;
    this.notifs = this.mainslides;
    this.notifs = this.notifs.filter(function (el) {
      return el.subject.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 || el.message.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

  searchitem(){
    if(this.ishidden == false){
      this.ishidden = true;
    }
    else{
      this.ishidden = false;
    }
  }

  refresh() {
    this.refreshPage();
  }

  async refreshPage() {
    const values = {
      notifs: 1
    };

    let headers = {
        
        'Authorization': 'Bearer ' + this.access_token
    };

    let msg = '';
    if(this.selectedLanguage==='en'){
      msg = 'Loading notifications. Please wait…😊';
    }else{
      msg = "Chargement des notifications. S'il vous plaît, attendez…😊";
    }
    const loader = await this.loading.create({
      message: msg,
    });

    loader.present().then(() => {

      this.HTTP.post('https://lithafrica.com/api/auth/notifications', values, headers)
        .then(data => {
          loader.dismiss();
          this.notifs = JSON.parse(data.data)['message'];
          this.storage.set('notifications', JSON.stringify(this.notifs));
          this.mainslides = this.notifs;
        })
        .catch(error => {
          loader.dismiss();
          if(this.selectedLanguage === 'en'){
            alert('Sorry, an error occurred');
          }else{
            alert("Désolé, une erreur s'est produite");
          }
        });

    });
  }

  refreshme(event){
    this.refresh();
    event.target.complete();
  }

  show(notif){
    
  }
}
