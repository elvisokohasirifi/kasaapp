import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AngularFirestore} from '@angular/fire/firestore';
import { TranslateConfigService } from '../translate-config.service';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.page.html',
  styleUrls: ['./progress.page.scss'],
})
export class ProgressPage implements OnInit {
  progress;
  alphabet;
  numbers;
  nouns;
  pronouns;
  conjunctions;
  adjectives;
  verbs;
  items;
  total;
  selectedLanguage: string;

  constructor(
    private storage: Storage, 
    public afStore: AngularFirestore, 
    private translateConfigService: TranslateConfigService, 
    public helper: HelperService) { 
      this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
    }


  ionViewDidEnter(){
    this.selectedLanguage = this.translateConfigService.currentLang();
    this.nouns = 0;
    //this.adjectives = 0;
    this.alphabet = 0;
    this.verbs = 0;
    this.pronouns = 0;
    this.numbers = 0;
    //this.conjunctions = 0;
    this.total = 0;

    this.progress = this.helper.progress;
    for (var i = this.progress.nouns.modules.length - 1; i >= 0; i--) {
      if(this.progress.nouns.modules[i].status == 'Completed') {
        this.nouns +=1;
        this.total +=1;
      }
    }
    
    /*for (var i = this.progress.adjectives.modules.length - 1; i >= 0; i--) {
      if(this.progress.adjectives.modules[i].status == 'Completed') {
        this.adjectives +=1;
        this.total +=1;
      }
    }*/

    for (var i = this.progress.verbs.modules.length - 1; i >= 0; i--) {
      if(this.progress.verbs.modules[i].status == 'Completed') {
        this.verbs +=1;
        this.total +=1;
      }
    }

    for (var i = this.progress.pronouns.modules.length - 1; i >= 0; i--) {
      if(this.progress.pronouns.modules[i].status == 'Completed') {
        this.pronouns +=1;
        this.total +=1;
      }
    }

    if(this.progress.alphabet.status == 'Completed') {
      this.alphabet +=1;
      this.total +=1;
    }

    if(this.progress.numbers.status == 'Completed') {
      this.numbers +=1;
      this.total +=1;
    }

    /*if(this.progress.conjunctions.status == 'Completed') {
      this.conjunctions +=1;
      this.total +=1;
    }*/

    this.items = [
      {en: "The Alphabet", fr: "L'alphabet", sets: 1, completed: this.alphabet},
      {en: "Numbers", fr: "Nombres", sets: 1, completed: this.numbers},
      {en: "Pronouns", fr: "Pronoms", sets: 5, completed: this.pronouns},
      {en: "Nouns", fr: "Noms", sets: 12, completed: this.nouns},
      {en: "Verbs", fr: "Verbes", sets: 7, completed: this.verbs},
      //{en: "Adjectives", fr: "Adjectifs", sets: 3, completed: this.adjectives},
      //{en: "Conjunctions", fr: "Conjonctions", sets: 1, completed: this.conjunctions}
    ]

  }

  ngOnInit() {
    
  }

}
