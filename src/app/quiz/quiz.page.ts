import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController, IonRadioGroup } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { TranslateConfigService } from '../translate-config.service';
import { HelperService } from '../services/helper.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.page.html',
  styleUrls: ['./quiz.page.scss'],
})
export class QuizPage implements OnInit {
  en;
  fr;
  uid;
  cur=1;
  type;
  items;
  sounds;
  answers;
  content;
  progress;
  lessons = this.helper.lessons.content;
  selectedLanguage: string;
  showtwiinquiz: boolean;
  @ViewChild(IonSlides, { static:true } ) slides: IonSlides;
  @ViewChild('theanswer', { static:true } ) answer: IonRadioGroup;
  constructor(
    private nav: NavController, 
    private storage: Storage, 
    private translateConfigService: TranslateConfigService, 
    public helper: HelperService,
    public afStore: AngularFirestore,) { 
    this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
    //this.helper.progress = this.helper.progress;
  }

  ionViewDidEnter(){
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit() {
    this.slides.lockSwipes(true);
    if(this.helper.quiztype == 'text'){
      this.type = 0;
    }
    else if(this.helper.quiztype == 'audio'){
      this.type = 1;
    }
    
    if(this.lessons[this.helper.index] && this.lessons[this.helper.index].children == null){
      this.items = this.lessons[this.helper.index].quiz;  
      this.showtwiinquiz = this.lessons[this.helper.index].showtwiinquiz;    
      this.en = this.lessons[this.helper.index].en;
      this.fr = this.lessons[this.helper.index].fr;

    }else{
      this.items = this.lessons[this.helper.index].children[this.helper.subidx].quiz;
      this.showtwiinquiz = this.lessons[this.helper.index].children[this.helper.subidx].showtwiinquiz;
      this.en = this.lessons[this.helper.index].children[this.helper.subidx].en;
      this.fr = this.lessons[this.helper.index].children[this.helper.subidx].fr;
    }

    this.uid = this.helper.userid;
    this.items = this.shuffleans(this.items);
    this.sounds  = this.lessons[this.helper.index].sounds;
    this.items = this.items.slice(0, 10); // select only 10 items for the quiz
    this.answers = new Array(this.items.length).fill(0); // where correct answers will be stored

    for (var i = this.items.length - 1; i >= 0; i--) {
      this.items[i]['ans'] = this.shuffleans(this.items[i]['ans']);
    }
  }

  shuffleans(array) {
    var m = array.length, t, i;
  
    // While there remain elements to shuffle
    while (m) {
      // Pick a remaining element…
      i = Math.floor(Math.random() * m--);
  
      // And swap it with the current element.
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }
  
    return array;
  }

  choose(event){
    if (event.detail.value.startsWith("t")) { // if the answer is correct
      this.slides.getActiveIndex().then(index =>{
        this.answers[index] = 1;
      });
    }
  }

  play(url){
    var audio = new Audio(this.sounds + url);
    audio.play();
  }

  next(){
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.cur+=1;
    this.slides.lockSwipes(true);
  }

  previous(){
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    if(this.cur>1){
      this.cur-=1;
    }
    this.slides.lockSwipes(true);
  }

  tostart(){
    this.slides.lockSwipes(false);
    this.slides.slideTo(0);
    this.cur = 1;
    this.slides.lockSwipes(true);
  }

  stop(){
    this.nav.pop();
  }

  toend(){
    this.slides.lockSwipes(false);
    this.cur = this.items.length+1;
    this.slides.slideTo(this.items.length);
    this.slides.lockSwipes(true);
  }

  finish(){
    let correct = this.answers.filter(function(value){
        return value == 1;
    }).length;
    let total = this.answers.length;
    let lesson = this.lessons[this.helper.index].en.toLowerCase();
    let score = Math.round(((correct/total)/2) * 10) / 10;

    if(score == 0.5){ // if you got everything right
      if(this.helper.subidx == null){
        if(lesson == 'alphabet') // there is only one quiz on alphabets
          score = 1;
        this.helper.progress[lesson].assessment_score += score;
        score = this.helper.progress[lesson].assessment_score;
      }else{
        this.helper.progress[lesson].modules[this.helper.subidx].assessment_score += score;
        score = this.helper.progress[lesson].modules[this.helper.subidx].assessment_score;
      }
    }
    if(score >= 1){
      if(this.helper.subidx == null){
        this.helper.progress[lesson].status = 'Completed';
      }else{
        this.helper.progress[lesson].modules[this.helper.subidx].status = 'Completed';
      }  
    }

    var docRef = this.afStore.collection('users').doc(this.uid);
    docRef.get().toPromise().then(doc => {
      if (doc.exists) {
        docRef.update({progress: this.helper.progress});
      }})
    this.storage.set('score', correct);
    this.storage.set('total', total);
    this.nav.navigateForward('score');
  }

}
