import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HelperService } from '../services/helper.service';
import { Storage } from '@ionic/storage';
import { TranslateConfigService } from '../translate-config.service';

@Component({
  selector: 'app-score',
  templateUrl: './score.page.html',
  styleUrls: ['./score.page.scss'],
})
export class ScorePage implements OnInit {
	score: any;
	image: string;
  	total: any;
  	message: string;
  	link: string;
  	selectedLanguage: string;
    lessons = this.helper.lessons.content;
  constructor(private storage:Storage, private nav: NavController,
    public helper: HelperService,
  	private translateConfigService: TranslateConfigService) { 
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit(){
    Promise.all([this.storage.get("score"), this.storage.get("total")]).then(values => {
      this.score = values[0];
      this.total = values[1];

      if(this.score == this.total) {
      	this.image = 'complete.svg';
      	if(this.selectedLanguage == 'en'){
          this.message = 'Congratulations! You scored ' + this.score + ' / ' + this.total + ". You have successfully completed this lesson";
        }else{
          this.message = 'Toutes nos félicitations! Vous avez marqué ' + this.score + ' / ' + this.total + ". Vous avez terminé avec succès cette leçon";
        }
      }
      else if(this.score > this.total/2){
      	this.image = 'happy.svg';
      	if(this.selectedLanguage == 'en'){ 
          this.message = 'Well done! You scored ' + this.score + ' / ' + this.total + ". You scored more than average, but you need to get everything right before you complete this lesson.";
        }else{
          this.message = 'Bien fait! Vous avez marqué ' + this.score + ' / ' + this.total + ". Vous avez obtenu un score supérieur à la moyenne, mais vous devez tout faire correctement avant de terminer cette leçon.";
        }
      }
      else{
      	this.image = 'sad.svg';
      	if(this.selectedLanguage == 'en'){
          this.message = 'You tried! You scored ' + this.score + ' / ' + this.total + ". You scored less than average. Take your time and go through the lesson again. We believe you will do very well next time. You need to get everything right before you complete this lesson.";
        }else{
          this.message = 'Tu as essayé! Vous avez marqué ' + this.score + ' / ' + this.total + ". Vous avez obtenu un score inférieur à la moyenne. Prenez votre temps et reprenez la leçon. Nous pensons que vous ferez très bien la prochaine fois. Vous devez tout bien comprendre avant de terminer cette leçon.";
        }
      }
    });
  }

  start(){
    /*
    if(this.helper.subidx!=null){
      this.helper.navContent(this.helper.index,this.helper.subidx)
    }else{
      if(this.helper.index == 0){
        this.nav.navigateForward('alphabet');
      }else{
        this.helper.navContent(this.helper.index, this.helper.subidx);
      }
    }*/
    this.nav.pop();
  }

}
