import { Injectable } from '@angular/core';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore} from '@angular/fire/firestore';
import { TranslateConfigService } from '../translate-config.service';
import lessons from "./lessons.json";

@Injectable({
  providedIn: 'root'
})
export class HelperService {
	name;
  email;
  phone;
  userid;
  theme;
  password;
  lessons;
  subidx = null;
  index;
  quiztype;

  progress = {
    alphabet: {
      status: 'Incomplete',
      assessment_score: 0
    },
    numbers: {
      status: 'Incomplete',
      assessment_score: 0
    },
    conjunctions: {
      status: 'Incomplete',
      assessment_score: 0
    },
    nouns: {
      modules: [
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        }
      ]
    },
    verbs: {
      modules: [
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        }
      ]
    },
    adjectives: {
      modules: [
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        }
      ]
    },
    pronouns: {
      modules: [
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        },
        {
          status: 'Incomplete',
          assessment_score: 0
        }
      ]
    }
  };
  selectedLanguage: string;
 

  constructor(private nav: NavController,
  						private alertController: AlertController, 
  						private storage: Storage, 
  						public afStore: AngularFirestore,
    					public ngFireAuth: AngularFireAuth, 
              private translateConfigService: TranslateConfigService, 
              private loading: LoadingController) {
      this.lessons = lessons;
      this.loadall();
  }

  loadall(){
    // read values from local storage
    Promise.all([this.storage.get("name"), 
      this.storage.get("email"), 
      this.storage.get("phone"), 
      this.storage.get("userid"), 
      this.storage.get("language"), 
      this.storage.get("progress")]).then(values => {    
        this.name = values[0];
        this.email = values[1];
        this.phone = values[2];
        this.userid = values[3];
        if(values[3] && values[5])
          this.progress = values[5];
        if(values[4]) {
          this.selectedLanguage = values[4];
          this.translateConfigService.setLanguage(this.selectedLanguage);
        }
        else
          this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
    });
  }

  async login(email, password){
    let msg = '';
    if(this.selectedLanguage==='en'){
      msg = 'Logging you in';
    }else{
      msg = "Vous connecter";
    }
    let loader = await this.loading.create({
      message: msg, 
    });

    loader.present().then(() => {
      this.ngFireAuth.signInWithEmailAndPassword(email, password).then((res) => {        
        var docRef = this.afStore.collection('users').doc(res.user.uid);
        this.save('userid', res.user.uid);
        this.userid = res.user.uid;
        docRef.get().toPromise().then(doc => {
          if (doc.exists) {
            this.saveandcontinue(doc);
          } else {
            this.addNewUserToFirestore(res, 'New User', '')
            loader.dismiss();
            if(this.selectedLanguage==='en'){
              alert('We do not have your details so we generated some for you. You can change them later');
            }else{
              alert("Nous n'avons pas vos coordonnées, nous en avons donc généré pour vous. Vous pouvez les changer plus tard")
            }
          }
        }).catch(function(error) {
          loader.dismiss();
            if(this.selectedLanguage==='en'){
              alert("Error getting document: " + error);
            }else{
              alert("Erreur lors de l'obtention du document" + error);
            }
        });      
      }).catch((error) => {
        loader.dismiss();
        alert(error.message);
      });
      loader.dismiss();
  });
  }

  saveandcontinue(doc){
    this.save('email', doc.data().email);
    this.email = doc.data().email;
    this.save('name', doc.data().name);
    this.name = doc.data().name;
    this.progress = doc.data().progress;
    this.save('progress', this.progress);
    this.save('phone', doc.data().phone);
    this.phone = doc.data().phone;
    this.save('loggedin', true);
    this.home('/tabs/tab1');
  }

  async signup(name, email, phone, password){
    this.clear();
    let msg = '';
    if(this.selectedLanguage==='en'){
      msg = 'Signing you up';
    }else{
      msg = "Vous inscrire";
    }
    let loader = await this.loading.create({
      message: msg,
    });

    loader.present().then(() => {
      this.ngFireAuth.createUserWithEmailAndPassword(email, password).then((res) => {        
        var docRef = this.afStore.collection('users').doc(res.user.uid);
        docRef.get().toPromise().then(doc => {
          if (!doc.exists) {
            this.addNewUserToFirestore(res, name, phone);
          }
          loader.dismiss();
        })
      .catch(error => {
        loader.dismiss();
        if(this.selectedLanguage==='en'){
          alert('Checking if customer exists failed ' + error);
        }else{
          alert("Échec de la vérification de l'existence du client " + error);
        }
        
        console.log(error);
        return;
      });
        this.home('login');
      }).catch((error) => {
        loader.dismiss();
        alert(error.message);
      });
    });
  }

  SendVerificationMail() {
    return this.ngFireAuth.currentUser.then(u => u.sendEmailVerification())
    .then(() => {
      this.home('login');
    })
  }

  deleteuser(){
    this.afStore.collection('users').doc(this.userid).delete().then(function() {
	    //alert("Account successfully deleted!");
      //this.ngFireAuth.deleteUser(this.userid).then(function() {
      this.ngFireAuth.currentUser.delete().then(function() {
        if(this.selectedLanguage==='en'){
          this.presentAlert('Account successfully deleted');
        }else{
          this.presentAlert('Compte supprimé avec succès');
        }
        this.home('welcome');
      })
      .catch(function(error) {
        if(this.selectedLanguage==='en'){
          alert("Error deleting auth account:" + JSON.stringify(error));
        }else{
          alert("Erreur lors de la suppression du compte d'authentification" + JSON.stringify(error));
        }
      });
      this.ngFireAuth.authState.first().subscribe((authState) => { authState.delete(); });
    }).catch(function(error) {
      if(this.selectedLanguage==='en'){
        alert("Error deleting user account: " + JSON.stringify(error));
      }else{
        alert('Erreur lors de la suppression du compte utilisateur: ' + JSON.stringify(error))
      }
    });
  }

  addNewUserToFirestore(res, name, phone) {
    this.name = name;
    this.phone = phone;
    this.email = res.user.email;
    this.save('name', name);
    this.save('email', this.email);
    this.save('phone', phone);
    this.save('progress', this.progress);
    

    this.afStore.collection('users').doc(res.user.uid)
      .set({
          name: name,
          phone: phone,
          email: res.user.email,
          progress: this.progress

      })
      //ensure to catch any errors at this stage to advise us if something does go wrong
      .catch(error => {
          if(this.selectedLanguage === "en"){
            alert('Something went wrong with adding user to firestore: ' + JSON.stringify(error));
          }else{
            alert("Une erreur s'est produite lors de l'ajout de l'utilisateur à firestore: " + JSON.stringify(error));
          }
          return;
      });
  }

  forgotpassword(email){
  	this.ngFireAuth.sendPasswordResetEmail(email).then(() => {
  			if(this.selectedLanguage==='en'){
          alert('Password Reset Link Sent');
        }else{
          alert('Lien de réinitialisation du mot de passe envoyé');
        }
  		})
      .catch((error) => {
        alert(error.message);
      });
  }

  navigate(url){
  	this.nav.navigateForward(url);
    //this.router.navigate([url]); 
  }

  back(){
  	this.nav.pop();
  }

  home(url){
    this.nav.navigateRoot(url);
  }

  async presentAlert(title, message) {
    const alert = await this.alertController.create({
      subHeader: title,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  save(key, value){
    this.storage.set(key, value);
  }
  
  async get(key){
    let value = null;
    await this.storage.get(key).then((val) => {
      value = val;
    });
    return value;
  }

  clear(){
    this.storage.clear();
    this.name = null;
    this.email = null;
    this.phone = null;
  }

  async saveprogress(){
    let msg = '';
    if(this.selectedLanguage==='en'){
      msg = 'Saving. Please wait…😊';
    }else{
      msg = "enregistrement. S'il vous plaît, attendez…😊";
    }
    let loader = await this.loading.create({
      message: msg,
    });
    
    loader.present().then(() => {
      var docRef = this.afStore.collection('users').doc(this.userid);
        docRef.get().toPromise().then(doc => {
          if (doc.exists) {
            loader.dismiss();
            docRef.update({
                progress: this.progress
            });
            if(this.selectedLanguage==='en'){
              alert('Saved');
            }else{
              alert('Enregistré');
            }
          } 
          else{
            loader.dismiss();
            if(this.selectedLanguage==='en'){
              alert('You cannot save your data. You are signed in anonymously');
            }else{
              alert('Vous ne pouvez pas enregistrer vos données. Vous êtes connecté anonymement');
            }
          }
        })
       .catch(error => {
           loader.dismiss();
            if(this.selectedLanguage==='en'){
              alert('An error occurred, please try again later');
            }else{
              alert('Un erreur est survenue, veuillez réessayer plus tard ')
            }
            alert(JSON.stringify(error));
            return;
        });
     });
  }

  navSubtopics(index){
    this.index = index;
    this.navigate('subtopics');
  }

  navContent(index, subidx=null){
    this.index = index;
    this.subidx = subidx;
    this.navigate('content');
  }

  navQuiz(type){
    this.quiztype = type;
    this.navigate('quiz');
  }

  signout(){
    this.ngFireAuth.signOut().then(() => {
      this.clear();
      this.home('welcome');
    })
    .catch(error => {
        //JSON.stringify(error);
        this.clear();
        this.home('welcome');
    });
  }

}
