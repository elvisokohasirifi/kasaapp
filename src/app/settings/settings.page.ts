import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HelperService } from '../services/helper.service';
import { TranslateConfigService } from '../translate-config.service';
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  selectedLanguage: string;
  dark: boolean;
  name: string;
  gender: string;

  constructor(private storage:Storage, private nav: NavController, private alertctrl: AlertController,
  	private translateConfigService: TranslateConfigService, public helper: HelperService) { 
    //this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
  }

  ionViewDidEnter(){
    this.selectedLanguage = this.helper.selectedLanguage;
  }

  ngOnInit() {
    Promise.all([this.storage.get("name"), this.storage.get("gender"), this.storage.get("dark")]).then(values => {
      this.name = values[0];
      this.gender = values[1];
      this.dark = values[2];
      if(this.dark == null)
        this.dark = false;
      //document.body.classList.toggle('dark', this.dark);
      //alert(this.dark);
    });
  }

  darkmode(){
  	this.dark = !this.dark;
  	this.storage.set('dark', !this.dark);
  	document.body.classList.toggle('dark', !this.dark);
  }

  signout(){
    let msg = '';
    if(this.selectedLanguage==='en'){
      msg = 'Are you sure you want to sign out? Your progress will be saved';
    }else{
      msg = 'Êtes-vous certain de vouloir vous déconnecter? Votre progression sera sauvegardée';
    }
    if (!confirm(msg)) {
      return;
    }
    this.helper.signout();
  }

  deleteuser(){
    let msg = '';
    if(this.selectedLanguage==='en'){
      msg = 'Are you sure you want to delete your account?';
    }else{
      msg = 'Êtes-vous sûr de vouloir supprimer votre compte?';      
    }
    if (!confirm(msg)) {
      return;
    }
    this.helper.deleteuser();
  }

  async changeLanguage() {
    const alert = await this.alertctrl.create({
      header: 'Language / Langue',
      inputs: [
        {
          name: 'english',
          type: 'radio',
          label: 'English',
          value: 'en'
        },
        {
          name: 'french',
          type: 'radio',
          label: 'Français',
          value: 'fr'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'OK',
          handler: (data:string) => {
            this.storage.set('language', data);
            this.selectedLanguage = data;
    		this.translateConfigService.setLanguage(this.selectedLanguage);
          }
        }
      ]
    });

    await alert.present();
  }

}
