import { Component, OnInit, ViewChild } from '@angular/core';
import { HelperService } from '../services/helper.service';
import { TranslateConfigService } from '../translate-config.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  @ViewChild('name', { static: true }) name;
  @ViewChild('email', { static: true }) email;
  @ViewChild('phone', { static: true }) phone;
  @ViewChild('password', { static: true }) password;
  error;
  ishidden: boolean;
  selectedLanguage:string;

  constructor(private translateConfigService: TranslateConfigService, public helper: HelperService) { 
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  
  ngOnInit() {
  	this.ishidden = true;
    // console.log(this.helper.progress);
  }

  signup(){
  	this.error = '';
  	if (this.name.value == '' || /\d/.test(this.name.value)) {
      this.error = this.selectedLanguage==='en' ? 'Please enter a valid name' : 'Entrez un nom valide';
      return;
    }
    if (this.name.value.length < 3 || !this.name.value.includes(' ')) {
      this.error = this.selectedLanguage==='en' ? 'Please enter your full name' : 'Entrez votre nom complet';
      return;
    }   
    if (this.email.value == '') {
      this.error = this.selectedLanguage==='en' ? 'Please enter a valid email' : 'Entrez un e-mail valide';
      return;
    }
    if (this.password.value == '') {
      this.error = this.selectedLanguage==='en' ? 'Please enter a valid password': 'Entrez un mot de passe valide';
      return;
    }
    if (this.password.value.length < 6) {
      this.error = this.selectedLanguage==='en' ? 'Password must have at least 6 characters' : 'Le mot de passe doit avoir au moins 6 caractères';
      return;
    }
    this.ishidden = false;
    this.helper.signup(this.name.value, this.email.value, this.phone.value, this.password.value);
    this.ishidden = true;
  }


}