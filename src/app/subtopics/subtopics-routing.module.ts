import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubtopicsPage } from './subtopics.page';

const routes: Routes = [
  {
    path: '',
    component: SubtopicsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubtopicsPageRoutingModule {}
