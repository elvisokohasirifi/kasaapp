import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { SubtopicsPageRoutingModule } from './subtopics-routing.module';

import { SubtopicsPage } from './subtopics.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubtopicsPageRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [SubtopicsPage]
})
export class SubtopicsPageModule {}
