import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SubtopicsPage } from './subtopics.page';

describe('SubtopicsPage', () => {
  let component: SubtopicsPage;
  let fixture: ComponentFixture<SubtopicsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubtopicsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SubtopicsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
