import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import { TranslateConfigService } from '../translate-config.service';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-subtopics',
  templateUrl: './subtopics.page.html',
  styleUrls: ['./subtopics.page.scss'],
})
export class SubtopicsPage implements OnInit {
  en;
  fr;
  children;
  lessons = this.helper.lessons.content;
  selectedLanguage: string;
  constructor(private nav: NavController, private translateConfigService: TranslateConfigService, public helper: HelperService, private route: ActivatedRoute) { 
    this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
  }

  ionViewDidEnter(){
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit() {
    this.children = this.lessons[this.helper.index].children;
    this.en = this.lessons[this.helper.index].en;
    this.fr = this.lessons[this.helper.index].fr;
  }

  content(subidx){
    this.helper.navContent(this.helper.index, subidx);
  }

}
