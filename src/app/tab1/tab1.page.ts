import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateConfigService } from '../translate-config.service';
import { NavController } from '@ionic/angular';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{
  en: string;
  fr: string;
  gender:string;
  dark: boolean;
  image: string;
  level: string;
  selectedLanguage: string;
  mainslides: any;

  lessons = this.helper.lessons.content;
  dp: string;

  items = [
      {en: "The Alphabet", fr: "L'Alphabet", image: "alphabet2.svg", sets: 1, link: 0, level: 'A1'},
      {en: "Numbers", fr: "Nombres", image: "calculator2.svg", sets: 1, link: 1, level: 'A1'},
      {en: "Pronouns", fr: "Pronoms", image: "pronouns2.svg", sets: 5, link: 4, level: 'B1'},
      {en: "Nouns", fr: "Noms", image: "nouns2.svg", sets: 12, link: 3, level: 'B1'},
      {en: "Verbs", fr: "Verbes", image: "verbs2.svg", sets: 7, link: 6, level: 'B1'},
      //{en: "Adjectives", fr: "Adjectifs", image: "adjectives2.svg", sets: 3, link: 5, level: 'B1'},
      //{en: "Adverbs", fr: "Adverbes", image: "adverbs2.svg", sets: 1, link: 7, level: 'B1'},
      //{en: "Conjunctions", fr: "Conjonctions", image: "conjunctions2.svg", sets: 1, link: 2, level: 'B1'},
  ];


  @ViewChild('search', { static: true }) search;

  constructor(private translateConfigService: TranslateConfigService, 
    public helper: HelperService, 
    private nav: NavController) { 
    this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
  }

  ionViewDidEnter(){
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit(){
    this.mainslides = this.items;
    if (this.helper.theme == 'dark') {
      document.body.classList.toggle('dark', true);
    }
    else{
      document.body.classList.toggle('dark', false);
    }

    this.dp = 'https://ui-avatars.com/api/?name=' + encodeURIComponent(this.helper.name) + '&color=EC4899&background=fff1f8'
  }

  searchByKeyword(){
    var searchTerm = this.search.value;
    this.items = this.mainslides;
    
    this.items = this.items.filter(function (el) {
      return el.en.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 || el.fr.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

  next(index){
    let item = this.lessons[index];
    if(item.en == 'Alphabet'){
      this.helper.index = index;
      this.helper.navigate('alphabet');
    }else{
      if(item.children == null){
        this.helper.navContent(index);
      }
      else{
        this.helper.navSubtopics(index);
      }
    } 
  }

  testClick(){}
}
