import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { TranslateConfigService } from '../translate-config.service';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
	private readonly tawkChatLink : string = 'https://tawk.to/chat/5efa517b4a7c6258179b915f/default/?$_tawk_popout=true';
  selectedLanguage: string;
  constructor(private storage: Storage, private iab: InAppBrowser, private translateConfigService: TranslateConfigService, public helper: HelperService) {
    this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
  }

  ionViewDidEnter(){
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit() {
  }

  chat(){
    const browser = this.iab.create(this.tawkChatLink, '_blank', {location:'no',usewkwebview:'yes',toolbar:'yes'}); 
  }
}