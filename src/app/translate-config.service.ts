import { Injectable } from '@angular/core';
import { TranslateService, TranslatePipe } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class TranslateConfigService {

  constructor(private translate: TranslateService) { }

  getDefaultLanguage(){
    let language = this.translate.getBrowserLang();
    if (language != 'en' && language != 'fr') {
      language = 'en';
    }
    this.translate.setDefaultLang(language);
    return language;
  }

  setLanguage(setLang) {
    this.translate.use(setLang);
  }

  currentLang(){
    return this.translate.currentLang;
  }
}

