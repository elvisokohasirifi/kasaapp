import { Component, OnInit } from '@angular/core';
import { TranslateConfigService } from '../translate-config.service';
import { HelperService } from '../services/helper.service';
// import { AuthenticationService } from "../shared/authentication-service";

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.page.html',
  styleUrls: ['./verify-email.page.scss'],
})
export class VerifyEmailPage implements OnInit {
  selectedLanguage: string;

  constructor(private translateConfigService: TranslateConfigService, public helper: HelperService) { 
    this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
  }

  ionViewDidEnter(){
    this.selectedLanguage = this.translateConfigService.currentLang();
  }

  ngOnInit() {
  }

}
