import { Component, OnInit, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { TranslateConfigService } from '../translate-config.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  @ViewChild('language', {static:true}) language;
  selectedLanguage:string;
  constructor(private storage:Storage, private nav: NavController, 
    private translateConfigService: TranslateConfigService) { 
    this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
  }

  ngOnInit() {
  }

  start(){
    this.changeLanguage();
    this.nav.navigateForward('login');
  }

  changeLanguage(){
    this.storage.set('language', this.language.value);
    this.selectedLanguage = this.language.value;
    this.translateConfigService.setLanguage(this.selectedLanguage);
  }
}
